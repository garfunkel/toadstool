package main

//go:generate go run include/include.go -o resources/resources.go favicon.ico templates static

import (
	"./resources"
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	"goji.io"
	"goji.io/pat"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

type ArgumentType int
type ArgumentValueType int
type ArgumentActionType int

const (
	MAX_UINT = ^uint(0)
	MAX_INT  = int(MAX_UINT >> 1)

	UNKNOWN ArgumentType = iota
	POSITIONAL
	FLAG

	STRING ArgumentValueType = iota
	INT
	FLOAT
	BOOL
	FILE

	STORE ArgumentActionType = iota
	STORE_TRUE
	STORE_FALSE
)

var (
	reImport *regexp.Regexp = regexp.MustCompile(`^[\t ]*?(` +
		`import\b.*?\b(?P<module>argparse)\b([\t ]*as[\t ]*(?P<as>\w+))?|` +
		`from[\t ]*(?P<module2>argparse)[\t ]*import\b.*?\b(?P<class2>ArgumentParser)\b([\t ]*as[\t ]*(?P<as2>\w+))?).*$`)
	Scripts     []Script
	CurrentJobs sync.Map
	JobCounter  = uint32(0)
	WorkingDir = filepath.Join(os.TempDir(), "toadstool")
	UserPaths []string
)

func subMatchesToMap(re *regexp.Regexp, subMatches []string) (match map[string]string) {
	match = make(map[string]string)

	for i, name := range re.SubexpNames() {
		if i != 0 && i < len(subMatches) && name != "" && subMatches[i] != "" {
			if strings.HasSuffix(name, "2") {
				name = name[:len(name)-1]
			}

			match[name] = subMatches[i]
		}
	}

	return
}

func humanise(input string) string {
	reParts := regexp.MustCompile(`([A-Z]+|[a-z]+|[\d]+)`)
	reUpper := regexp.MustCompile(`[A-Z]+`)
	reLower := regexp.MustCompile(`[a-z]+`)
	matches := reParts.FindAllString(input, -1)

	for index, match := range matches {
		if index < len(matches)-1 {
			if reUpper.MatchString(match) && reLower.MatchString(matches[index+1]) {
				runes := []rune(match)
				matches[index] = string(runes[:len(runes)-1])
				matches[index+1] = string(runes[len(runes)-1]) + matches[index+1]
			}
		}
	}

	return strings.Title(strings.Join(matches, " "))
}

type Job struct {
	Id       uint
	Script   *Script
	Path     string
	Started  time.Time
	Finished time.Time
	//ExitCode - Go 1.12
}

type CodeParser interface {
	parse(scriptPath string) (desc string, args Arguments, err error)
}

type Script struct {
	Path     string
	Name     string
	Desc     string
	HashBang string
	Args     Arguments
}

type Arguments []Argument

type Argument struct {
	Name      string
	LongName  string
	HumanName string
	Help      string
	Type      ArgumentType
	Action    ArgumentActionType
	MinValues int
	MaxValues int
	ValueType ArgumentValueType
	Desc      string
	//Values   []interface{}
}

type PythonParser struct {
}

func (parser *PythonParser) cleanString(s string) string {
	if strings.HasPrefix(s, `"""`) && strings.HasSuffix(s, `"""`) {
		s = s[2 : len(s)-2]
	} else if strings.HasPrefix(s, "'''") && strings.HasSuffix(s, "'''") {
		s = s[2 : len(s)-2]
	}

	unquoted, err := strconv.Unquote(s)

	if err != nil {
		return s
	}

	return unquoted
}

//const default choices required metavar dest
func (parser *PythonParser) stringToArgument(argString string) (arg Argument, err error) {
	regexes := []struct {
		Name  string
		Regex *regexp.Regexp
	}{
		{
			Name:  "string",
			Regex: regexp.MustCompile(`(?s:^("""("""|.*?[^\\]""")|'''('''|.*?[^\\]''')|"("|.*?[^\\]")|'('|.*?[^\\]')))`),
		}, {
			Name:  "float",
			Regex: regexp.MustCompile(`^-?\d*(\.\d+)?`),
		}, {
			Name:  "int",
			Regex: regexp.MustCompile(`^-?\d+`),
		}, {
			Name:  "hex",
			Regex: regexp.MustCompile(`^-?0x[\dA-Fa-f]+`),
		}, {
			Name:  "oct",
			Regex: regexp.MustCompile(`^-?0[0-8]+`),
		}, {
			Name:  "bool",
			Regex: regexp.MustCompile(`^(True|False)\b`),
		}, {
			Name:  "assignment",
			Regex: regexp.MustCompile(`^=`),
		}, {
			Name:  "comma",
			Regex: regexp.MustCompile(`^,`),
		}, {
			Name:  "whitespace",
			Regex: regexp.MustCompile(`^\s+`),
		}, {
			Name:  "id",
			Regex: regexp.MustCompile(`^[A-Za-z_]\w*`),
		},
	}

	pos := 0
	lastId := ""

	for pos < len(argString) {
		for _, regex := range regexes {
			match := regex.Regex.FindString(argString[pos:])

			if match == "" {
				continue
			}

			if regex.Name == "id" {
				if lastId == "type" {
					if match == "str" {
						arg.ValueType = STRING
					} else if match == "int" {
						arg.ValueType = INT
					} else if match == "float" {
						arg.ValueType = FLOAT
					}
				}

				lastId = match
			} else if regex.Name == "string" {
				cleanMatch := parser.cleanString(match)

				if lastId == "" {
					if strings.HasPrefix(cleanMatch, "--") {
						arg.Type = FLAG
						arg.LongName = cleanMatch
					} else if strings.HasPrefix(cleanMatch, "-") {
						arg.Type = FLAG
						arg.Name = cleanMatch
					} else {
						arg.Type = POSITIONAL
						arg.LongName = cleanMatch
					}
				} else if lastId == "action" {
					if cleanMatch == "store_true" {
						arg.ValueType = BOOL
						arg.Action = STORE_TRUE
					} else if cleanMatch == "store_false" {
						arg.ValueType = BOOL
						arg.Action = STORE_FALSE
					}
				} else if lastId == "nargs" {
					if cleanMatch == "?" {
						arg.MinValues = 0
						arg.MaxValues = 1
					} else if cleanMatch == "*" {
						arg.MinValues = 0
						arg.MaxValues = MAX_INT
					} else if cleanMatch == "+" {
						arg.MinValues = 1
						arg.MaxValues = MAX_INT
					}
				} else if lastId == "help" {
					arg.Help = cleanMatch
				} else if lastId == "description" {
					arg.Desc = cleanMatch
				}
			} else if regex.Name == "int" {
				if lastId == "nargs" {
					var n int

					n, err = strconv.Atoi(match)

					if err == nil {
						arg.MinValues = n
						arg.MaxValues = n
					} else {
						arg.MinValues = 1
						arg.MaxValues = 1

						err = nil
					}
				}
			}

			pos += len(match)

			break
		}
	}

	if arg.Name == "" && arg.LongName != "" {
		arg.Name = arg.LongName
	}

	if arg.Type == FLAG {
		arg.HumanName = strings.TrimLeft(arg.LongName, "-")
	} else {
		arg.HumanName = arg.LongName
	}

	arg.HumanName = humanise(arg.HumanName)

	return
}

func (parser *PythonParser) parse(scriptPath string) (desc string, args Arguments, err error) {
	file, err := os.Open(scriptPath)

	if err != nil {
		return
	}

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		text := scanner.Text()
		trimmedText := strings.TrimSpace(text)

		if desc == "" {
			if trimmedText == "" || strings.HasPrefix(trimmedText, "#") {
				continue
			} else if strings.HasPrefix(trimmedText, `"""`) {
				desc += text
			} else {
				break
			}

			if trimmedText != `"""` && strings.HasSuffix(trimmedText, `"""`) {
				break
			}
		} else {
			desc += "\n" + text

			if strings.HasSuffix(trimmedText, `"""`) {
				break
			}
		}
	}

	if _, err = file.Seek(0, 0); err != nil {
		return
	}

	desc = strings.TrimSpace(strings.Replace(desc, `"""`, "", -1))
	scanner = bufio.NewScanner(file)
	var importMatch map[string]string

	for scanner.Scan() {
		text := scanner.Text()
		importMatch = subMatchesToMap(reImport, reImport.FindStringSubmatch(text))
		_, ok := importMatch["module"]

		if !ok {
			continue
		}

		if len(importMatch) > 0 {
			break
		}
	}

	var reObject *regexp.Regexp
	var objectMatch map[string]string
	module, ok := importMatch["module"]
	fmt.Println(importMatch)

	if !ok {
		err = errors.New("No argparse module imported.")
		return
	}

	class, ok := importMatch["class"]
	as, ok := importMatch["as"]

	if ok {
		if class == "" {
			module = as
		} else {
			class = as
		}
	}

	if class == "" {
		reObject = regexp.MustCompile(fmt.Sprintf(`^[\t ]*(?P<object>\w+)[\t ]*=[\t ]*(?P<module>%v)\.(?P<class>ArgumentParser)[\t ]*\((?P<args>.*?)\)[\s#]*$`, module))
	} else {
		reObject = regexp.MustCompile(fmt.Sprintf(`^[\t ]*(?P<object>\w+)[\t ]*=[\t ]*(?P<class>%v)[\t ]*\((?P<args>.*?)\)[\s#]*$`, class))
	}

	for scanner.Scan() {
		text := scanner.Text()
		objectMatch = subMatchesToMap(reObject, reObject.FindStringSubmatch(text))

		if len(objectMatch) > 0 {
			break
		}
	}

	if len(objectMatch) == 0 {
		err = errors.New("No ArgumentParser object instantiated.")
	}

	fmt.Println(objectMatch)

	reArg := regexp.MustCompile(fmt.Sprintf(`^[\t ]*(?P<object>%v)\.add_argument\((?P<args>.*?)\)[\s#]*$`, objectMatch["object"]))

	for scanner.Scan() {
		text := scanner.Text()
		argMatch := subMatchesToMap(reArg, reArg.FindStringSubmatch(text))

		if len(argMatch) > 0 {
			fmt.Println(argMatch)
			var arg Argument

			arg, err = parser.stringToArgument(argMatch["args"])

			if err != nil {
				return
			}

			args = append(args, arg)

			fmt.Printf("%+v\n", arg)
		}
	}

	return
}

func parseCode(path string) (script Script, err error) {
	var parser CodeParser

	if strings.HasSuffix(path, ".py") {
		parser = new(PythonParser)
	}

	desc, args, err := parser.parse(path)

	fmt.Println(args)

	path, err = filepath.Abs(path)

	if err != nil {
		log.Fatal(err)
	}

	script = Script{
		Path: path,
		Name: filepath.Base(path),
		Args: args,
		Desc: desc,
	}

	return
}

func HandleIndex(w http.ResponseWriter, r *http.Request) {
	page, err := template.New("index").Parse(resources.R["templates/index.html"])

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	data := struct {
		Scripts []Script
	}{
		Scripts: Scripts,
	}

	page.Execute(w, data)
}

func HandleStatic(w http.ResponseWriter, r *http.Request) {
	value, ok := resources.R[strings.TrimLeft(r.URL.Path, "/")]

	if ok {
		ext := filepath.Ext(r.URL.Path)

		switch ext {
		case ".css":
			w.Header().Add("Content-Type", "text/css")
		}

		fmt.Fprintf(w, value)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func HandleScript(w http.ResponseWriter, r *http.Request) {
	scriptName := pat.Param(r, "script")
	page, err := template.New("script").Parse(resources.R["templates/script.html"])

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		fmt.Println(err)

		return
	}

	var script Script

	for _, script = range Scripts {
		if script.Name == scriptName {
			break
		}
	}

	if script.Name != scriptName {
		w.WriteHeader(http.StatusNotFound)

		return
	}

	data := struct {
		Script      Script
		INT         ArgumentValueType
		FLOAT       ArgumentValueType
		BOOL        ArgumentValueType
		STORE_TRUE  ArgumentActionType
		STORE_FALSE ArgumentActionType
	}{
		Script:      script,
		INT:         INT,
		FLOAT:       FLOAT,
		BOOL:        BOOL,
		STORE_TRUE:  STORE_TRUE,
		STORE_FALSE: STORE_FALSE,
	}

	page.Execute(w, data)
}

func HandleScriptRun(w http.ResponseWriter, r *http.Request) {
	scriptName := pat.Param(r, "script")

	if err := r.ParseMultipartForm(1000000); err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	var script Script

	for _, script = range Scripts {
		if script.Name == scriptName {
			break
		}
	}

	if script.Name != scriptName {
		w.WriteHeader(http.StatusNotFound)

		return
	}

	argIndex := 0
	cmd := []string{
		script.Path,
	}

	for {
		var argValues []string

		for key, value := range r.MultipartForm.Value {
			if key == fmt.Sprintf("arg_%v", argIndex) {
				argValues = value

				break
			}
		}

		if len(argValues) == 0 {
			for key, _ := range r.MultipartForm.File {
				if key == fmt.Sprintf("arg_%v", argIndex) {
					// save file somewhere and value becomes that path

					argValues = []string{"a file path"}

					break
				}
			}
		}

		if len(argValues) == 0 {
			break
		}

		arg := script.Args[argIndex]

		if arg.Type == FLAG {
			cmd = append(cmd, arg.Name)
		}

		cmd = append(cmd, argValues...)

		argIndex++
	}

	jobId := uint(atomic.AddUint32(&JobCounter, 1))
	jobPath := filepath.Join(WorkingDir, "jobs", fmt.Sprintf("%v", jobId))

	if info, err := os.Stat(jobPath); os.IsNotExist(err) {
		err = os.MkdirAll(jobPath, 0755)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)

			return
		}
	} else if info.IsDir() {
		dir, err := os.Open(jobPath)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)

			return
		}

		defer dir.Close()

		names, err := dir.Readdirnames(-1)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)

			return
		}

		for _, name := range names {
			err = os.RemoveAll(filepath.Join(jobPath, name))

			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)

				return
			}
		}
	} else {
		err = os.RemoveAll(jobPath)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)

			return
		}

		err = os.MkdirAll(jobPath, 0755)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)

			return
		}
	}

	job := Job{
		Id:      jobId,
		Script:  &script,
		Started: time.Now().UTC(),
		Path:    jobPath,
	}

	CurrentJobs.Store(job.Id, job)

	fmt.Println(job)

	go func() {
		fmt.Println(cmd)

		err := os.MkdirAll(filepath.Join(jobPath, "input"), 0755)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)

			return
		}

		err = ioutil.WriteFile(filepath.Join(jobPath, "input", "cmd"), []byte(strings.Join(cmd, " ")), 0644)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)

			return
		}

		err = os.MkdirAll(filepath.Join(jobPath, "output"), 0755)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)

			return
		}

		outFile, err := os.Create(filepath.Join(jobPath, "output", "stdout"))

		if err != nil {
			fmt.Println(err)
		}

		defer outFile.Close()

		errFile, err := os.Create(filepath.Join(jobPath, "output", "stderr"))

		if err != nil {
			fmt.Println(err)
		}

		defer errFile.Close()

		exe := exec.Command(cmd[0], cmd[1:]...)
		exe.Stdout = outFile
		exe.Stderr = errFile
		err = exe.Start()

		if err != nil {
			fmt.Println(err)

			return
		}

		err = exe.Wait()
		job.Finished = time.Now().UTC()

		CurrentJobs.Store(job.Id, job)

		if err != nil {
			fmt.Println(err)
		}
	}()

	response, err := json.Marshal(job)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	w.Write(response)
}

func HandleJob(w http.ResponseWriter, r *http.Request) {
	jobParam, err := strconv.ParseUint(pat.Param(r, "job"), 10, 64)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	jobId := uint(jobParam)
	page, err := template.New("job").Parse(resources.R["templates/job.html"])

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		fmt.Println(err)

		return
	}

	var job Job

	CurrentJobs.Range(func(key, value interface{}) bool {
		if jobId == key.(uint) {
			job = value.(Job)

			return false
		}

		return true
	})

	if job.Id == 0 {
		w.WriteHeader(http.StatusNotFound)

		return
	}

	fmt.Println(job)

	data := struct {
		Job Job
	}{
		Job: job,
	}

	page.Execute(w, data)
}

func HandleJobSocket(w http.ResponseWriter, r *http.Request) {
	jobParam, err := strconv.ParseUint(pat.Param(r, "job"), 10, 64)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)

		return
	}

	jobId := uint(jobParam)

	var job Job

	CurrentJobs.Range(func(key, value interface{}) bool {
		if jobId == key.(uint) {
			job = value.(Job)

			return false
		}

		return true
	})

	if job.Id == 0 {
		w.WriteHeader(http.StatusNotFound)

		return
	}

	var upgrader = websocket.Upgrader{
		ReadBufferSize:  1000,
		WriteBufferSize: 1000,
	}

	socket, err := upgrader.Upgrade(w, r, nil)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	fmt.Println(job)

	// stdin
	go func() {
		file, err := os.OpenFile(filepath.Join(job.Path, "input", "stdin"), os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)

		if err != nil {
			fmt.Println(err)

			return
		}

		defer file.Close()

		for {
			var msg struct {
				Type string
				Msg  string
			}

			err := socket.ReadJSON(&msg)

			if err != nil {
				return
			}

			if _, err = file.WriteString(msg.Msg); err != nil {
				fmt.Println(err)
			}

			msg.Type = "stdinReceived"
			msg.Msg = ""

			if err = socket.WriteJSON(msg); err != nil {
				fmt.Println(err)
			}
		}
	}()

	// stdout/stderr
	stdoutFile, err := os.Open(filepath.Join(job.Path, "output", "stdout"))

	if err != nil {
		fmt.Println(err)

		return
	}

	defer stdoutFile.Close()

	stderrFile, err := os.Open(filepath.Join(job.Path, "output", "stderr"))

	if err != nil {
		fmt.Println(err)

		return
	}

	defer stderrFile.Close()

	for {
		value, ok := CurrentJobs.Load(jobId)
		updatedJob := value.(Job)

		for {
			stderr := make([]byte, 1000)
			numBytes, err := stderrFile.Read(stderr)

			if err == io.EOF {
				break
			} else if err != nil {
				fmt.Println(err)

				return
			}

			if numBytes > 0 {
				msg := struct {
					Type string
					Msg  string
				}{
					Type: "stderr",
					Msg:  string(stderr[:numBytes]),
				}

				if err = socket.WriteJSON(msg); err != nil {
					return
				}
			}
		}

		for {
			stdout := make([]byte, 1000)
			numBytes, err := stdoutFile.Read(stdout)

			if err == io.EOF {
				break
			} else if err != nil {
				fmt.Println(err)

				return
			}

			if numBytes > 0 {
				msg := struct {
					Type string
					Msg  string
				}{
					Type: "stdout",
					Msg:  string(stdout[:numBytes]),
				}

				if err = socket.WriteJSON(msg); err != nil {
					return
				}
			}
		}

		if !ok || !updatedJob.Finished.IsZero() {
			err = socket.Close()

			if err != nil {
				fmt.Println(err)
			}

			break
		}
	}
}

func HandleFiles(w http.ResponseWriter, r *http.Request) {
	files, err := filepath.Glob(pat.Param(r, "path") + "*")

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	if files == nil {
		files = []string{}
	}

	safeFiles := []string{}
	var safeMap = make(map[string]string)

	for _, userPath := range UserPaths {
		info, err := os.Stat(userPath)

		if err != nil {
			continue
		}

		if info.IsDir() {
			safeMap[userPath] = "dir"
		} else {
			safeMap[userPath] = "file"
		}
	}

	for _, file := range files {
		for userPath, pathType := range safeMap {
			if file == userPath {
				safeFiles = append(safeFiles, file)

				break
			} else if pathType == "dir" {
				if strings.Contains(file, strings.TrimRight(userPath, string(os.PathSeparator)) + string(os.PathSeparator)) {
					safeFiles = append(safeFiles, file)

					break
				}
			}
		}
	}

	data, err := json.Marshal(safeFiles)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)

		return
	}

	w.Write(data)
}

func usage() {
	fmt.Printf("" +
		"Usage: toadstool [OPTION]...\n" +
		"Web application for running scripts and programs.\n\n" +
		"Options:\n" +
		"  -w, --working-dir=DIR   working directory (default: '%v')\n" +
		"  -s, --scripts=FILES     script files/directories to load\n" +
		"  -u, --user-paths=FILES  files/directories to enable for user input suggestions\n\n",
		WorkingDir)
}

func main() {
	var scriptPaths []string
	var userPaths []string
	var state string

	for _, arg := range os.Args[: ] {
		if arg == "-h" || arg == "--help" {
			usage()

			os.Exit(0)
		} else if arg == "-s" || arg == "--scripts" {
			state = "scripts"

			continue
		} else if arg == "-u" || arg == "--user-paths" {
			state = "userPaths"
		} else if strings.HasPrefix(arg, "--") {
			fmt.Printf("" +
				"toadstool: unrecognised option -- '%v'\n" +
				"Usage: toadstool [OPTION]...\n" +
				"Try 'toadstool --help' for more information.\n",
				arg)

			os.Exit(2)
		} else if strings.HasPrefix(arg, "-") {
			if len(arg) > 1 {
				fmt.Printf("" +
					"toadstool: invalid option -- '%v'\n" +
					"Usage: toadstool [OPTION]...\n" +
					"Try 'toadstool --help' for more information.\n",
					string([]rune(arg)[1]))

				os.Exit(2)
			}
		} else if state == "scripts" {
			scriptPaths = append(scriptPaths, arg)
		} else if state == "userPaths" {
			userPaths = append(userPaths, arg)
		}
	}

	for _, path := range scriptPaths {
		script, err := parseCode(path)

		if err != nil {
			log.Fatal(err)
		}

		Scripts = append(Scripts, script)
	}

	for _, path := range userPaths {
		UserPaths = append(UserPaths, path)
	}

	mux := goji.NewMux()
	mux.HandleFunc(pat.Get("/"), HandleIndex)
	mux.HandleFunc(pat.Get("/favicon.ico"), HandleStatic)
	mux.HandleFunc(pat.Get("/static/*"), HandleStatic)
	mux.HandleFunc(pat.Get("/script/:script"), HandleScript)
	mux.HandleFunc(pat.Post("/script/:script/run"), HandleScriptRun)
	mux.HandleFunc(pat.Get("/job/:job"), HandleJob)
	mux.HandleFunc(pat.Get("/job/:job/socket"), HandleJobSocket)
	mux.HandleFunc(pat.Get("/files/:path"), HandleFiles)

	http.ListenAndServe(":8080", mux)
}
