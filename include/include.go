package main

import (
	"encoding/base64"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

var (
	FS   = make(map[string]string)
	CODE = `package resources

import (
	"encoding/base64"
	"log"
)

var (
	R = %#v
)

func init() {
	for key, value := range R {
		data, err := base64.StdEncoding.DecodeString(value)

		if err != nil {
			log.Fatal(err)
		}

		R[key] = string(data)
	}
}
`
)

func usage() {
	fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
	flag.PrintDefaults()
	fmt.Fprintf(flag.CommandLine.Output(), "  path [path...]\n")
}

func walkFn(path string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}

	if !info.IsDir() {
		data, err := ioutil.ReadFile(path)

		if err != nil {
			return err
		}

		FS[path] = base64.StdEncoding.EncodeToString(data)
	}

	return nil
}

func main() {
	outputPath := flag.String("o", "resources.go", "output Go module path")
	flag.Usage = usage
	flag.Parse()

	if len(flag.Args()) == 0 {
		return
	}

	for _, arg := range flag.Args() {
		if err := filepath.Walk(arg, walkFn); err != nil {
			log.Fatal(err)
		}
	}

	err := os.MkdirAll(filepath.Dir(*outputPath), 0777)

	if err != nil {
		log.Fatal(err)
	}

	file, err := os.Create(*outputPath)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(file, CODE, FS)
}
